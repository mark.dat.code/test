export { default as HomePage } from './home/Home';
export { default as ProfilePage } from './profile/Profile';
export { default as TasksPage } from './tasks/tasks';
export { default as OrdersPage } from './orders/Orders';
export { default as OrderListPage } from './orders/list/OrderList';
export { default as PaymentPage } from './payment/Payment';
export { default as SettingPage } from './setting/Setting';
