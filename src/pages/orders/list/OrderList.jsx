import { OrderList } from "@components";

import "./OrderList.scss";

export default function OrderListPage() {
  return (
    <>
      <OrderList />
    </>
  );
}
