import { lazy, Suspense } from 'react';
import { Navigate } from 'react-router-dom';

const routes = [
	{
        path: '/dashboard',
        element: <WrapLazyRoute LazyElement={lazy(() => import("@pages/home/Home"))} />
    },
	{
        path: '/members',
        element: <WrapLazyRoute LazyElement={lazy(() => import("@pages/members/Members"))} />
    },
	{
		path: '/orders',
		element: <WrapLazyRoute LazyElement={lazy(() => import("@pages/orders/Orders"))} />,
		children: [
			{
				index: true,
				element: <WrapLazyRoute LazyElement={lazy(() => import("@pages/orders/list/OrderList"))} />,
			},
			{
				path: ':orderId',
				element: <WrapLazyRoute LazyElement={lazy(() => import("@pages/orders/details/OrderDetails"))} /> 
			}
		]
	},
	{
		path: '/payment',
		element: <WrapLazyRoute LazyElement={lazy(() => import("@pages/payment/Payment"))} />
	},
	{
		path: '/setting',
		element:  <WrapLazyRoute LazyElement={lazy(() => import("@pages/setting/Setting"))} />
	},
	{
		path:'*',
		element: <Navigate to='/orders' />
	}
];


// lazy loading
export function WrapLazyRoute({LazyElement}) {

	return (
		<Suspense fallback="Loading...">
			<LazyElement />
		</Suspense>
	);
}

export default routes;

