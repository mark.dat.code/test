import { Popup } from 'devextreme-react';

import { getScreenSize } from '@utils/media-query';
import {default as FoodMenuForm} from '../../form/FoodMenuForm';

function FoodMenuPopup({onHiding, isVisible = false}) {
    const [sizeScreen]  = useState(getScreenSize()); 

    const handleOnHiding = () => {
        onHiding();
    }

	return <>
        <Popup
          visible={isVisible}
          onHiding={handleOnHiding}
          dragEnabled={false}
          hideOnOutsideClick={true}
          showCloseButton={false}
          showTitle={true}
          title="Food Menu"
          container=".dx-viewport"
          width={900}
          height={700}
          fullScreen={sizeScreen.isXSmall}
        >
            <FoodMenuForm />
        </Popup>
	</>
}


export default memo(FoodMenuPopup);
