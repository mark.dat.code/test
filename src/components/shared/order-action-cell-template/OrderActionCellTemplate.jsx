import './OrderActionCellTemplate.scss'

export default function ActionCellTemplate({event, onEditClicked, onDeleteClicked, onOrderClicked}) {

    const handleEditClicked = (e) => {
        onEditClicked(e, event.data);
    }

    const handleDeleteClicked = (e) => {
        onDeleteClicked(e, event.data);
    }

    const handleOrderClicked = (e) => {
        onOrderClicked(e, event.data);
    }
    
    return (
        <>
            <div className="action-cell">
                <i onClick={handleOrderClicked} className="action-cell--edit action-cell-hover fa-solid fa-burger-soda"></i>
                <i onClick={handleEditClicked} className="action-cell--edit action-cell-hover fa-regular fa-pen-to-square"></i>
                <i onClick={handleDeleteClicked} className="action-cell--delete action-cell-hover fa-regular fa-trash"></i>
            </div>
        </>
      );
}
