import { Form } from 'devextreme-react';
import { GroupItem, SimpleItem } from 'devextreme-react/form';

function OrderInformForm({formData}) {

    return <>
        <div className='order-inform-form'>
            <Form formData={formData}>
                <GroupItem colCount={2}>
                    <GroupItem caption="Thông tin đơn">
                        <SimpleItem dataField="gstRate" label={{ text: 'Địa chỉ' }} />
                        <GroupItem colCount={2}>
                            <SimpleItem dataField="gstRate" label={{ text: 'Trạng thái đơn' }} />
                            <SimpleItem dataField="gstRate" label={{ text: 'Loại order' }} />
                        </GroupItem>
                        <SimpleItem dataField="gstRate" label={{ text: 'Chú thích' }} />
                    </GroupItem>

                    <GroupItem caption="Hạn">
                        <SimpleItem dataField="gstRate" label={{ text: 'Ngày tạo đơn' }} editorOptions={{ readOnly: true }} />
                        <GroupItem colCount={2}>
                            <SimpleItem dataField="gstRate" label={{ text: 'Giờ bắt đầu' }} />
                            <SimpleItem dataField="gstRate" label={{ text: 'Giờ hết hạn' }} />
                        </GroupItem>
                    </GroupItem>
                </GroupItem>
            </Form>
        </div>
    </>
}


export default memo(OrderInformForm);