import { OrderInformForm } from '@components/shared';
import './OrderForm.scss'

function OrderForm() {
	const [formData] = useState({});

	return <>
		<div className="order-form">
			<div className="order-food">
				<div className='order-food__title'>
					<span>Title</span>
				</div>
				<div className='order-food__content'>
					<div className='order-content'>
						<div className='order-content__inform'>
							<OrderInformForm formData={formData}/>
						</div>
						<div className='order-content__orders'>
							Bang
						</div>
					</div>
				</div>
			</div>
		</div>
	</>
}


export default memo(OrderForm);
