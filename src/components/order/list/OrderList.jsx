import DataGrid, {
	Column,
	FilterRow,
	Lookup,
	Pager,
	Paging
} from "devextreme-react/data-grid";

import { FoodMenuPopup, OrderActionCellTemplate } from "@components/shared";

export default function OrderList() {
	const navigate = useNavigate();
	const [isShowMenuPopup, setShowMenuPopup] = useState(false);

	const onEditClicked = (e, data) => {
		console.log(data);
		navigate('/orders/1');
	};

	const onDeleteClicked = (e, data) => {
		console.log(data);
	};

	const onOrderClicked = (e, data) => {
		setShowMenuPopup(true);
	};

	const onMenuPopupHiding = () => {
		setShowMenuPopup(false);
	}

	return (
		<>
			<DataGrid
				className={"dx-card wide-card"}
				dataSource={dataSource}
				showBorders={false}
				focusedRowEnabled={false}
				defaultFocusedRowIndex={0}
				columnAutoWidth={true}
				columnHidingEnabled={false}
				hoverStateEnabled={true}
				wordWrapEnabled={true}
			>
				<Paging defaultPageSize={10} />
				<Pager showPageSizeSelector={true} showInfo={true} />
				<FilterRow visible={true} />

				<Column
					dataField={"id"}
					width={100}
					caption={"Mã"}
					alignment="center"
				/>
				<Column
					dataField={"restaurantName"}
					width={200}
					caption={"Tên nhà hàng"}
				/>
				<Column dataField={"status"} caption={"Trạng thái"} />
				<Column
					dataField={"owner"}
					caption={"Người tạo"}
					alignment={'left'}
				/>
				<Column
					dataField={"createDate"}
					caption={"Ngày tạo"}
					dataType={"datetime"}
				/>
				<Column
					dataField={"expiredDate"}
					caption={"Ngày hết hạn"}
					dataType={"datetime"}
				/>
				<Column dataField={"personOrderNumber"} caption={"Số người đã đặt"} width={200}>
					<Lookup
						dataSource={priorities}
						valueExpr={"value"}
						displayExpr={"name"}
					/>
				</Column>

				<Column
					width={80}
					fixed={true}
					fixedPosition="right"
					cellRender={(e) => (
						<OrderActionCellTemplate
							event={e}
							onOrderClicked={onOrderClicked}
							onEditClicked={onEditClicked}
							onDeleteClicked={onDeleteClicked}
						/>
					)}
				/>
			</DataGrid>

			<FoodMenuPopup isVisible={isShowMenuPopup} onHiding={onMenuPopupHiding} />
		</>
	);
}

const dataSource = [
	{
		id: 1,
		restaurantName: 'Co tam',
		status: 'Đang chờ',
		owner: 'Quynh Nguyen',
		createDate: new Date(),
		personOrderNumber: 8,
		expiredDate: new Date()
	}
];

const priorities = [
	{ name: "High", value: 4 },
	{ name: "Urgent", value: 3 },
	{ name: "Normal", value: 2 },
	{ name: "Low", value: 1 },
];
